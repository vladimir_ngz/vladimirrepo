package com.crud.app.services;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import com.crud.app.dao.IEmployeeDAO;
import com.crud.app.models.Employee;

@Service
public class EmployeeService {

	@Autowired
	IEmployeeDAO empDao;

	List<Employee> employees = new ArrayList<>();

	//GET ALL EMPLOYEES
	public List<Employee> getAllEmployees(){
		return (List<Employee>) empDao.findAll();
	}

	//GET EMPLOYEE BY ID
	public Employee getEmployeeById(Long idEmp) {
		try {
			return empDao.findById(idEmp).get();	
		}
		catch(NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "====== The employee was not found in the database ======");
		}
	}
	
	//CREATE NEW EMPLOYEE/UPDATE NEW EMPLOYEE
	public Employee saveEmployee(Employee emp) {
		empDao.save(emp);
		return emp;
	}
	
	//DELETE EMPLOYEE
	public void deleteEmployee(Long idEmp){
		empDao.deleteById(idEmp);
	}

}
