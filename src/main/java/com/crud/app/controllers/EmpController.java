package com.crud.app.controllers;

import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.crud.app.models.Employee;
import com.crud.app.services.EmployeeService;

@RestController
@RequestMapping("/employees")
public class EmpController {

	@Autowired
	EmployeeService empService;

	//GET ALL EMPLOYEES
	@GetMapping
	public ResponseEntity<List<Employee>> getAllEmp(){
		return new ResponseEntity<List<Employee>>(empService.getAllEmployees(), HttpStatus.OK);
	}

	//GET EMPLOYEE BY ID
	@GetMapping(value = "/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long idEmp){		
		Employee employee = empService.getEmployeeById(idEmp);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	//CREATE EMPLOYEE
	@PostMapping
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee emp){
		return new ResponseEntity<Employee>(empService.saveEmployee(emp), HttpStatus.CREATED);
	}

	//UPDATE EMPLOYEE
	@PutMapping(value = "/{idEmployee}")
	public ResponseEntity<Employee> update(@PathVariable("idEmployee") Long idEmployee, @RequestBody Employee emp){
		try {
			Employee empUpd = empService.getEmployeeById(idEmployee);
			empUpd.setName(emp.getName());
			empUpd.setSalary(emp.getSalary());
			empUpd.setDoj(emp.getDoj());
			empUpd.setJoiningDate(emp.getJoiningDate());
			empUpd.setDesignation(emp.getDesignation());
			return new ResponseEntity<Employee>(empService.saveEmployee(empUpd), HttpStatus.OK);
		}
		catch(NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	//DELETE EMPLOYEE
	@DeleteMapping(value = "/{idEmp}")
	public ResponseEntity<Void> deleteEmployeeById(@PathVariable("idEmp") Long idEmp){
		empService.deleteEmployee(idEmp);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}


}
