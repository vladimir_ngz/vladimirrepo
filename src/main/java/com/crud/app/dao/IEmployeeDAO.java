package com.crud.app.dao;

import org.springframework.data.repository.CrudRepository;

import com.crud.app.models.Employee;

public interface IEmployeeDAO extends CrudRepository<Employee, Long>{
	
	
}
